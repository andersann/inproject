class LibrariesController < ApplicationController
  before_action :set_library, only: %i[show edit update destroy]

  def index
    @libraries = Library.all
  end

  def new
    @library = Library.new
    @library.address = Address.new
  end

  def create
    @library = Library.new(library_params)
    if @library.save
      redirect_to library_url(@library.id)
    else
      redirect_to libraries_url
    end
  end

  def update
    if @library.update(library_params)
      redirect_to library_url(@library.id)
    else
      redirect_to libraries_url
    end
  end

  def destroy
    if @library.destroy
      redirect_to libraries_url
    else
      redirect_to library_url(@library.id)
    end
  end

  private

  def set_library
    @library = Library.find(params[:id])
  end

  def library_params
    params.require(:library).permit(:name, address_attributes: %i[id street building])
  end

end
