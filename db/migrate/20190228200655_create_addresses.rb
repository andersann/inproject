class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street
      t.integer :building
      t.belongs_to :library, index: true

      t.timestamps
    end
  end
end
